# Шпаргалка по командам

## Minikube

[Документация](https://kubernetes.io/docs/setup/learning-environment/minikube/#specifying-the-vm-driver)


### Запуск

```bash
minikube start --driver=<driver_name>
```
drive_name = docker,virtualbox

### Остановка 

```bash
minikube stop
```

### Удаление  

```bash
minikube delete
```

### Настройка докер клиента для доступа в миникуб

```bash
minikube docker-env
```
Пример настроек:
```bash 
export DOCKER_TLS_VERIFY="1"
export DOCKER_HOST="tcp://192.168.64.2:2376"
export DOCKER_CERT_PATH="/Users/nedlosster/.minikube/certs"
export MINIKUBE_ACTIVE_DOCKERD="minikube"

# To point your shell to minikube's docker-daemon, run:
# eval $(minikube -p minikube docker-env)
```

После выставления переменных окружение клиент docker подключается к докер-демону миникуба.

### Запуск дашборда 

```bash
minikube dashboard 
```
### Ссылка на сервис

```bash
minikube service my-service 

nedlosster@nedlosster-ws ~ $ minikube service my-service 
|-----------|------------|-------------|-------------------------|
| NAMESPACE |    NAME    | TARGET PORT |           URL           |
|-----------|------------|-------------|-------------------------|
| default   | my-service |        8080 | http://172.17.0.2:31677 |
|-----------|------------|-------------|-------------------------|
🎉  Opening service default/my-service in default browser...

```

### Состояние ноды

```bash
kubectl describe node minikube
```
