#!/bin/sh

DIR=$(cd $(dirname $0) && pwd)

pushd $DIR/../..
helm install app-1-nodejs-pg app-1-nodejs-pg --debug --set service.type=NodePort --namespace develop
popd

export NODE_PORT=$(kubectl get --namespace develop -o jsonpath="{.spec.ports[0].nodePort}" services app-1-nodejs-pg)
export NODE_IP=$(kubectl get nodes --namespace develop -o jsonpath="{.items[0].status.addresses[0].address}")
echo http://$NODE_IP:$NODE_PORT