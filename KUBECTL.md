# KUBECTL - утилита управления кластером KUBERNETES

создание namespaces

```bash
kubectl create namespace develop
```
Посмотреть список namespaces

```bash
kubectl get namespace --all-namespaces
```

kubectx - переключение контекстов куба

Посмотреть список всех pods во всех namespaces

```bash
kubectl get pod --all-namespaces
```

Список deployments
```bash
kubectl get deployments --all-namespaces
```

Настройки deployment
```bash
kubectl describe deployment kubernetes-dashboard -n kubernetes-dashboard
```

#dashboard

```bash
kubectl proxy --address='192.168.30.11' --accept-hosts='.*'  -n kubernetes-dashboard
```