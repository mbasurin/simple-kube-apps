# HELM - пакетный менеджер для kubernetes

примеры для версии 3.

## Создание чарта

```bash
helm create app-1-nodejs-pg
```

## Инсталяция чарта

* --dry-run  симмуляция инсталяция
* --debug дополнительный отладочный выход

Инсталяция программы из чарта

```bash
helm install app-1-nodejs-pg ./app-1-nodejs-pg --dry-run --debug
```

Получить список релизов в кластере

```bash
helm list
```

Удалить релиз

```bash
helm uninstall app-1-nodejs-pg
```