#!/bin/bash

set -eu

mkdir -p tmp

vagrant up

vagrant ssh kub1 -c '/vagrant/setup_node.sh'
vagrant ssh kub2 -c '/vagrant/setup_node.sh'
vagrant ssh kub3 -c '/vagrant/setup_node.sh'
vagrant ssh kub1 -c '/vagrant/setup_cluster.sh'


#vagrant snapshot save clean_vms
