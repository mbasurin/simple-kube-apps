mkdir -p tmp
sudo kubeadm init --pod-network-cidr=10.244.0.0/16 --apiserver-advertise-address 192.168.30.11 | tee tmp/kubeadm_result.log
ADDHOST="sudo "`tail -n 2 tmp/kubeadm_result.log | tr -d '\r\n\\'`" --ignore-preflight-errors=all"

echo $ADDHOST > tmp/add_node_to_k8s.sh

mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

kubectl taint nodes --all node-role.kubernetes.io/master-
kubectl create clusterrolebinding add-on-cluster-admin --clusterrole=cluster-admin --serviceaccount=kube-system:default

##########  Выбираем CNI

#kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"

########################

cp tmp/add_node_to_k8s.sh /vagrant/tmp
sudo cp /etc/kubernetes/admin.conf /vagrant/tmp
#kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0/aio/deploy/alternative.yaml

kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0/aio/deploy/recommended.yaml